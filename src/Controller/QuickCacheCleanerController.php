<?php

namespace Drupal\quick_cache_cleaner\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Messenger\MessengerTrait;

/**
 * Defines the Quick Cache Cleaner controller.
 */
class QuickCacheCleanerController extends ControllerBase {

  use MessengerTrait;

  /**
   * Clear caches and redirect back to the admin/config page.
   */
  public function invoke() {
    drupal_flush_all_caches();
    $this->messenger()->addMessage($this->t('Caches cleared.'));
    return $this->redirect('system.admin_config');
  }

}
