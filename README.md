Quick Cache Cleaner
===================

Add an administrative menu item to quickly clear all Drupal caches.

Installation
------------

Quick Cache Cleaner can be installed like any other Drupal module,
then set module permissions to allow users to clear caches.
